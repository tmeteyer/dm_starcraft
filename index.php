<?php 

require "vendor/autoload.php";
use Michelf\Markdown;

$loader = new \Twig\Loader\FilesystemLoader(dirname(__FILE__) . '/views');

$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', '\Twig\Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new \Twig\Extension\DebugExtension()); // Add the debug extension
    
    $twig->addFilter(new \Twig\TwigFilter('markdown', function($string){
        return Markdown::defaultTransform($string);
    }));
});

Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});



Flight::route('/', function(){
    Flight::render('index.twig');
});



Flight::route('/race/@nomrace', function($nomrace){

    if($nomrace == "terran"){
        $data = array(
            'terrans' => get_terran(),
            'race' => $nomrace,
        );
    }elseif($nomrace == "zerg"){
        $data = array(
            'zergs' => get_zerg(),
            'race' => $nomrace,
        );
    }elseif($nomrace == "protoss"){
        $data = array(
            'protosss' => get_protoss(),
            'race' => $nomrace,
        );
    }
    
    Flight::render('race.twig', $data);
});


Flight::route('/@nomrace/@slug', function($nomrace, $slug){
    if($nomrace == "terran"){
        $data = array(
            'terranslug' => get_terran_by_slug($slug),
            'race' => $nomrace
        );
    }elseif($nomrace == "zerg"){
        $data = array(
            'zergslug' => get_zerg_by_slug($slug),
            'race' => $nomrace
        );
    }
    Flight::render('unite.twig', $data);
});

Flight::start();


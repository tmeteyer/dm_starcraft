<?php
function get_terran()
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/starcraft/terran/units/');
    if($response->status_code == 200)
    {
        return json_decode($response->body);
    }
    return array();
}

function get_terran_by_slug($slug)
{
    $url = sprintf("https://allosaurus.delahayeyourself.info/api/starcraft/terran/units/%s", $slug);
    $response = Requests::get($url);
    if($response->status_code == 200)
    {
        return json_decode($response->body);
    }
    return null;
}


function get_zerg()
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/starcraft/zerg/units/');
    if($response->status_code == 200)
    {
        return json_decode($response->body);
    }
    return array();
}

function get_zerg_by_slug($slug)
{
    $url = sprintf("https://allosaurus.delahayeyourself.info/api/starcraft/zerg/units/%s", $slug);
    $response = Requests::get($url);
    if($response->status_code == 200)
    {
        return json_decode($response->body);
    }
    return null;
}

function get_protoss()
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/starcraft/terran/units/');
    if($response->status_code == 200)
    {
        return json_decode($response->body);
    }
    return array();
}